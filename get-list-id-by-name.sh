#!/bin/bash

listName=$1

if [ -z $listName ]
then
	echo "You need to specify list name!"
	exit 13
fi

boardId=${TRELLO_FOOD_BOARD_ID}
oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

response=$(curl --silent --request GET --url "https://api.trello.com/1/boards/$boardId/lists?cards=none&card_fields=all&filter=open&fields=all&key=$key&token=$oauth")

while [[ $response == *"TOKEN"* ]]
do
	if [[ $response == *"TOKEN"* ]]
	then
		echo "$response ...RETRY after 2 seconds..."
		sleep 2s
		response=$(curl --silent --request GET --url "https://api.trello.com/1/boards/$boardId/lists?cards=none&card_fields=all&filter=open&fields=all&key=$key&token=$oauth")
	fi
done

listId=$(jq ".[] | select(.name | contains(\"$listName\")) | .id " <<< $response)
listId=$(sed -e 's/\"//g' <<< $listId)

echo $listId

