#!/bin/bash

listName=$1

if [ -z $listName ]
then
	echo "You need to specify list name!"
	exit 13
fi

oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

listId=$(. get-list-id-by-name.sh $listName)

response=$(curl --silent --request GET --url "https://api.trello.com/1/lists/$listId/cards?fields=id,name,badges,labels&checklists=all&key=$key&token=$oauth")
echo $response
