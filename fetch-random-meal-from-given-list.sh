#!/bin/bash

listName=$1

if [ -z $listName ]
then
	echo "You need to specify list name!"
	exit 10
fi

meals=$(. fetch-meals-from-given-list.sh $listName)

listLength=$(jq '. | length' <<< $meals)
randomMealIndex=$(($RANDOM % $listLength))

mealId=$(jq ".[$randomMealIndex].id" <<< $meals)
sed -e "s/\"//g" <<< $mealId
