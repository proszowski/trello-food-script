#!/bin/bash

day=$1

if [ -z $day ]
then
	echo "You need to specify day"
fi

. add-meal-to-day.sh Breakfast $day
. add-meal-to-day.sh Snacks $day
. add-meal-to-day.sh Lunch $day
. add-meal-to-day.sh Dinner $day
echo "$day" >> checkpoint
