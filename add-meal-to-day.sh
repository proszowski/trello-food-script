#!/bin/bash

listName=$1
day=$2

if [ -z $listName ]
then
	echo "You need to specify list name"
fi

if [ -z $day ]
then
	echo "You need to specify day"
fi


listId=$(. fetch-random-meal-from-given-list.sh $listName) 
. add-meal-to-list.sh $listId $day 
