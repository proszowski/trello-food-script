#!/bin/bash

declare -a days=("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday")

oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

for day in "${days[@]}"
do
	. archive-all-cards-in-list.sh $day
	. create-day-plan.sh $day &
done

if [ ! -f "checkpoint" ]; then
	touch checkpoint
fi

checkpointsNum=$(wc -l < checkpoint)
while [ $checkpointsNum -lt 7 ]
do
	sleep 5s
	checkpointsNum=$(wc -l < checkpoint)
	echo "...waiting for all checkpoints..."
done
. create-shopping-list.sh
rm checkpoint
