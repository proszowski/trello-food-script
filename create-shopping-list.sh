#!/bin/bash

oauth=${TRELLO_OAUTH_KEY}
trello_key=${TRELLO_API_KEY}

declare -a days=("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday")

if [ -f "list" ]; then
	rm "list"
fi

for day in "${days[@]}"
do
	response=$(. fetch-meals-from-given-list.sh $day)
	ingredients=$(jq '.[].checklists[].checkItems[].name ' <<< $response)
	ingredients=$(sed 's/\"//g' <<< $ingredients)
	echo "$ingredients" >> list
done

declare -A ingredients

list="list"
while IFS= read -r line
do
	echo $line
	if [ -z ${ingredients[$line]} ]; then
		ingredients[$line]=1
	else
		i=ingredients[$line]
		ingredients[$line]=$(($i+1))
	fi
done < $list

listName="to-buy"
. archive-all-cards-in-list.sh $listName
toBuyId=$(. get-list-id-by-name.sh $listName)
for key in "${!ingredients[@]}"; do
	if [[ $key != "0" ]]; then
		name="$key x${ingredients[$key]}"
		name=$(sed 's/\s/%20/g' <<< $name)
		curl --request POST --url "https://api.trello.com/1/cards?idList=$toBuyId&name=$name&key=$trello_key&token=$oauth" >& /dev/null
	fi
done

rm $list
