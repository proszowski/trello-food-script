#!/bin/bash

boardId=${TRELLO_FOOD_BOARD_ID}
oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

mealId=$1
listName=$2

if [ -z $mealId ]
then
	echo "You must specify meal id you want to copy"
	exit 5
fi

if [ -z $listName ]
then
	echo "You must specify list name to which you wanna copy a meal"
	exit 5
fi

listId=$(. get-list-id-by-name.sh $listName)

curl --request POST \
   --silent \
	--retry 3\
	--connect-timeout 5\
	--retry-delay 0 \
   --url "https://api.trello.com/1/cards?idList=$listId&idCardSource=$mealId&keepFromSource=all&key=$key&token=$oauth" >& /dev/null
