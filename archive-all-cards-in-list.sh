#!/bin/bash

oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

name=$1

if [ -z $name ]; then
	echo "You need to specify list name!"
fi

id=$(. get-list-id-by-name.sh $name)

curl --request POST \
  --url "https://api.trello.com/1/lists/$id/archiveAllCards?key=$key&token=$oauth" >& /dev/null &

